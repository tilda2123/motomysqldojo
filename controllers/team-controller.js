const Team = require('../models/Team');

const deleteTeam = (req, res, next) => {
    Team.delete(req.params.id, (error, results) => {
        if(error){
            res.render('error', {error});
        }
        res.redirect('/');
    });
}

module.exports = { deleteTeam };