const connection = require('./config');
const createTeam = new Promise(
    (resolve, reject) => connection.query(`CREATE TABLE team (
        id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(320) NOT NULL UNIQUE
    )`, (err) => {
        if(err) reject(err);
        console.log('Team table created');
        resolve();
    })
);
const createRider = new Promise(
    (resolve, reject) => connection.query(`CREATE TABLE rider (
        id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(320) NOT NULL,
        team_id INT NOT NULL,
        FOREIGN KEY (team_id) REFERENCES team(id) 
        ON DELETE CASCADE
    )`, (err) => {
        if(err) reject(err);
        console.log('rider table created');
        resolve();
    })
);
const createMotorcycle = new Promise(
    (resolve, reject) => connection.query(`CREATE TABLE motorcycle (
        id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(320) NOT NULL,
        rider_id INT NOT NULL,
        FOREIGN KEY (rider_id) REFERENCES rider(id)
        ON DELETE CASCADE
    )`, (err) => {
        if(err) reject(err);
        console.log('motorcycle table created');
        resolve();
    })
);

createTeam
    .then(() => 
        createRider
            .then(() => 
                createMotorcycle
                    .then(() =>
                    console.log('OK!')
                )
                .then(() => 
                    connection.end()
                )
            )
    )
    .catch(err => {
        console.log(err);
        connection.end();
    });


