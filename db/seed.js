const connection = require('./config');

const insertTeam = new Promise(
    (resolve, reject) => connection.query(`INSERT INTO team (name) VALUES ('Rocket'), ('Magic')
    `, (err) => {
        if(err) reject(err);
        console.log('Team inserted');
        resolve();
    })
);
const insertRider = new Promise(
    (resolve, reject) => connection.query(`INSERT INTO rider (name, team_id) VALUES ('Manuela', 1), ('Raquel', 2), ('Ines', 1), ('Matilde', 2), ('Adelina', 1), ('Milena', 2)
    `, (err) => {
        if(err) reject(err);
        console.log('rider inserted');
        resolve();
    })
);
const insertMotorcycle = new Promise(
    (resolve, reject) => connection.query(`INSERT INTO motorcycle (name, rider_id) VALUES ('Discovery', 1), ('Wolf', 1), ('Dreamer', 2), ('Guardian', 2), ('Gladiator', 3), ('Firestarter', 3), ('Infinity', 4), ('Visage', 4), ('Thunderer', 5), ('Freedom', 5), ('Inspiration', 6), ('Starlight', 6)
    `, (err) => {
        if(err) reject(err);
        console.log('motorcycle inserted');
        resolve();
    })
);

insertTeam
    .then(() => 
        insertRider
            .then(() => 
                insertMotorcycle
                    .then(() =>
                    console.log('OK!')
                )
                .then(() => 
                    connection.end()
                )
            )
    )
    .catch(err => {
        console.log(err);
        connection.end();
    });


