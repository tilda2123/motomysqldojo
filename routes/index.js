const express = require('express');
const router = express.Router();
const { showHomepage } = require('../controllers/pages-controller');
const { deleteTeam } = require('../controllers/team-controller');

/* GET home page. */
router.get('/', showHomepage);

router.post('/delete/:id', deleteTeam);

module.exports = router;
