const connection = require('../db/config');

class Team {
    constructor(){

    }
};

Team.delete = (id, callback) => {
    connection.query(
        'DELETE FROM team WHERE id = ?',
        [id], 
        (err, results) => {
            callback(err, results);
        }
    )
}


module.exports = Team;